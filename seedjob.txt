job("Checkin code") {
		description()
		scm {
			git {
				remote {
				  url("https://github.com/aashika0929/spring-music.git")
				  }
				branch("*/master")
			}
		}
	}


job("Code Quality") {
	description()
	scm {
		git {
			remote {
              url("https://github.com/aashika0929/spring-music.git")
             }
			branch("*/master")
		}
	}
  configure {
    it / 'builders' / 'hudson.plugins.sonar.SonarRunnerBuilder' {
      properties ''
      javaOpts ''
      jdk '(Inherit From Job)'
      project ''
      task 'scan'
    }
  }
}

job("upload artifacts to Nexus") {
	description()
	scm {
		git {
			remote {
              url("https://github.com/aashika0929/spring-music.git")
              }
			branch("*/master")
		}
	}
	steps {
      shell("curl -v -u nexususername:nexuspswd --upload-file readme  nexusurl/content/repositories/repokey/artifacts/1.0/readme")
    }
}



curl -v -u admin:admin123 \ --upload-file training.ui.apps-0.0.1-SNAPSHOT.zip \ http://docker-container.eastus.cloudapp.azure.com:8081/nexus/content/repositories/EVR/artifacts/2.0/training.ui.apps-0.0.1-SNAPSHOT.zip

 curl -v -u admin:admin123 \
    --upload-file training.ui.apps-0.0.1-SNAPSHOT.zip \
    http://docker-container.eastus.cloudapp.azure.com:8081/nexus/content/repositories/EVR/artifacts/3.0/training.ui.apps-0.0.1-SNAPSHOT.zip

	
--------add credentials in jenkins-----------	
curl -X POST 'http://rig:rig@52.168.175.97:8082/credentials/store/system/domain/_/createCredentials' \
--data-urlencode 'json={
  "": "0",
  "credentials": {
    "scope": "GLOBAL",
    "id": "sample",
    "username": "sample",
    "password": "sample",
    "description": "sample",
    "$class": "com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl"
  }
}'


curl -X POST https://a.blazemeter.com:443/api/v4/tests/6195713/start -H "Content-Type: application/json" --user '2a227e0cb8a7e74730d572bc:e22c50f9a722a182deb9508728a376a254e939197076fb5a617fba7413b8dcb2318cc358'




