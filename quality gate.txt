Quality gate
curl -u admin:admin -X POST http://wiprorig.eastus.cloudapp.azure.com:9000/api/qualitygates/project_status?projectKey=Project%3Acf-node > process.json
status=$(jq -r '.projectStatus.status' process.json)
if [ "$status" = "OK" ]
then
    exit 0
else
   exit 1
fi


Concourse CI
docker pull postgres
docker run --name concourse-db \
  --net=concourse-net \
  -h concourse-postgres \
  -p 5432:5432 \
  -e POSTGRES_USER=admin \
  -e POSTGRES_PASSWORD=admin \
  -e POSTGRES_DB=atc \
  -d postgres
  
docker pull concourse/concourse
docker run  --name concourse \
  -h concourse \
  -p 8080:8080 \
  --detach \
  --privileged \
  --net=concourse-net \
  concourse/concourse quickstart \
  --add-local-user=admin:admin \
  --main-team-local-user=admin \
  --external-url=http://18.217.34.39:8080 \
  --postgres-user=admin \
  --postgres-password=admin \
  --postgres-host=concourse-db \
  --worker-garden-dns-server 8.8.8.8
  
  fly -t tutorial execute -c task_hello_world.yml
  
  
 fly -t tutorial set-pipeline \
    --pipeline my-pipeline \
    --config task_hello_world.yml
	
	
fly set-team -n main \
    --basic-auth-username admin \
    --basic-auth-password admin
	

sudo concourse web \  
  --session-signing-key /etc/concourse/session_signing_key \
  --tsa-host-key /etc/concourse/tsa_host_key \
  --tsa-authorized-keys /etc/concourse/authorized_worker_keys \
  --postgres-user=concourse \
  --postgres-password=admin \
  --postgres-database=concourse atc \
  --external-url http://18.188.89.61:8080
